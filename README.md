# Ordnerstruktur der Dateien

Die nachstehende Tabelle zeigt den Inhalt dieses Repositories.<br>
Die zweite Spalte enthält sowohl Dateien als auch Unterordner. Dateien sind an ihren spezifischen Dateiendungen erkennbar, Ordner hingegen durch einen vorgestellten Backslash.



| Ordner                  | Inhalt                                           | Beschreibung |
| ----------------------- | ------------------------------------------------ | ------------ |
| \Masterarbeit           | Masterarbeit\_Graness\_1698691.pdf               | Masterarbeit im pdf-Format |
| \Kapitel_2              | Example\_ROC\_PR-Curve.ipynb                     | Jupyter Notebook mit Python-Code zum Erzeugen exemplarischer ROC- und PR-Kurven |
| \Kapitel_3              | \Code_&_Daten\_Visualisierungen                  | Ordner mit Python-Code und Daten zum Erzeugen der Visualisierungen aus Kapitel 3 |
|                         | \Code\_Datensatzerstellung                       | Ordner mit Python-Code für die Datenvorverarbeitung, das Labeling und die Erzeugung synthetischer Anomalien |
| \Kapitel_4              | \Deep-Learning-Methoden                          | Ordner mit Python-Code der modifizierten offiziellen Implementierung betrachteter Methoden |
|                         | \Identifikation\_realer\_Anomalien               | Ordner mit Daten zu den Anomalie-Scores bei der Identifikation realer Anomalien und Python-Code für die Auswertung |
|                         | Ergebnisse\_Methodenauswahl.xlsx                 | Ergebnisse des quantitativen Vergleichs der Deep-Learning-Methoden im xlsx-Format |
|                         | Ergebnisse\_PatchCore\_Optimierung.xlsx          | Ergebnisse zu der Optimierung des PatchCore auf dem Validierungsdatensatz im xlsx-Format |
|                         | Ergebnisse\_PatchCore\_Validierungs_&_Testdatensatz.xlsx | Ergebnisse des PatchCore mit der initialen und optimierten Konfiguration auf den Validierungs- und Testdaten sowie den erweiterten Testdaten im xlsx-Format |
| \Kapitel_4_&_5          | \Code_&_Daten\_Visualisierungen                  | Ordner mit Python-Code und Daten zum Erzeugen der Visualisierungen aus Kapitel 4 und 5 |
| \Kapitel_5              | \Code\_Erstellung\_Referenzdatensatz             | Ordner mit sämtlichem Python-Code für die Erstellung des Referenzdatensatzes |
|                         | \Referenzdatensatz                               | Ordner mit den Aufnahmen des originalen und vorverarbeiteten Referenzdatensatzes |
|                         | Ergebnisse\_PatchCore\_Referenzdatensatz.xlsx    | Ergebnisse des PatchCore auf dem Referenzdatensatz im xlsx-Format |
